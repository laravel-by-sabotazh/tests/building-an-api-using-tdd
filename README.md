## Introduction

The purpose of this project is to show that TDD doesn't have to be hard or be a pain to include as part of your workflow or your team's, you just need to understand where to start and what to test.

### Tutorial

From [laravel.io](https://laravel.io/articles/building-an-api-using-tdd-in-laravel).

```bash
php artisan test
```

### License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
